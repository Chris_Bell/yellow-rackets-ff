import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  build: {
    outDir: './public'
  },
  publicDir: './static',
  base: '/yellow-rackets-ff/'
})
