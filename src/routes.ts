import { RouteRecordRaw } from "vue-router";
import LeagueDashboard from './pages/LeagueDashboard.vue';
import NotFound from './pages/NotFound.vue';
import WeeklyMatchups from './pages/WeeklyMatchups.vue';
import CurrentStandings from './pages/CurrentStandings.vue';
import DraftOrder from './pages/DraftOrder.vue';
import PlayoffPicture from './pages/PlayoffPicture.vue';

import LeagueStatistics from './pages/LeagueStatistics.vue';

const routes: RouteRecordRaw[] = [
    {
        path: '/',
        component: LeagueDashboard,
        children: [
            {
                path: '',
                component: CurrentStandings,
            },
            {
                path: 'matchups',
                component: WeeklyMatchups,
            },
            {
                path: 'draft-order',
                component: DraftOrder,
            },
            {
                path: 'playoffs',
                component: PlayoffPicture,
            },
        ],
    },
    {
        path: '/stats',
        component: LeagueStatistics,
    },
    { path: '/:pathMatch(.*)*', name: 'NotFound', component: NotFound },
];

export default routes;