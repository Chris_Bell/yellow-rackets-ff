import { defineStore } from 'pinia';

import { useRootStore } from '../rootStore';

import { getTransactions } from '../../services/SleeperService';
import Transaction from '../../interfaces/Transaction';
import { createTransactions } from '../../factories/TransactionFactory';

interface State {
    Transactions: Transaction[];
    WeekLastFetched: number;
    IsFetchingData: boolean;
}

export const useTransactionsStore = defineStore('transactions', {
    state: (): State => ({
        Transactions: [],
        WeekLastFetched: 1,
        IsFetchingData: false,
    }),
    actions: {
        async getTransactionsForCurrentWeek() {
            const rootStore = useRootStore();
            this.IsFetchingData = true;
            this.WeekLastFetched = rootStore.CurrentWeek + 1;

            return this.getTransactionsForNextWeek();
        },
        async getTransactionsForNextWeek() {
            const rootStore = useRootStore();
            this.IsFetchingData = true;
            this.WeekLastFetched--;

            return getTransactions(this.WeekLastFetched)
                .then((response) => {
                    this.Transactions.push(...createTransactions(response, rootStore.Rosters, rootStore.Managers));

                    this.IsFetchingData = false;
                });
        },
    },
});