import { defineStore } from 'pinia';

import { useRootStore } from '../rootStore';

import Matchup from '../../models/Matchup';

import { createMatchups } from '../../factories/MatchupFactory';
import { getMatchups } from '../../services/SleeperService';

interface State {
    Matchups: Matchup[];
}

export const useMatchupsStore = defineStore('matchups', {
    state: (): State => ({
        Matchups: [],
    }),
    actions: {
        async getMatchupsForCurrentWeek() {
            const rootStore = useRootStore();
            rootStore.IsFetchingData = true;

            return getMatchups(rootStore.CurrentWeek)
                .then((response) => {
                    this.Matchups = createMatchups(response, rootStore.Rosters)
                        .sort((matchupA, matchupB) => matchupA.Id - matchupB.Id);

                        rootStore.IsFetchingData = false;
                });
        },
    },
});