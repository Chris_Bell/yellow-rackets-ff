import { defineStore } from 'pinia';
import PlayoffRound from '../../models/PlayoffRound';
import createPlayoffBracket, { addByeWeeks, createProjectedPlayoffBracket, createProjectedToiletBowl } from '../../factories/PlayoffBracketFactory';
import { getToiletBowl, getWinnersBracket } from '../../services/SleeperService';
import { useMatchupsStore } from './matchupsStore';
import { useRootStore } from '../rootStore';

interface State {
    WinnersBracket: PlayoffRound[];
    ToiletBowl: PlayoffRound[];
}

export const usePlayoffsStore = defineStore('playoffs', {
    state: (): State => ({
        WinnersBracket: [],
        ToiletBowl: [],
    }),
    actions: {
        async getWinnersBracket() {
            const rootStore = useRootStore();
            rootStore.IsFetchingData = true;

            return getWinnersBracket()
                .then((response) => {
                    if (rootStore.SeasonType == 'post') {
                        const matchupsStore = useMatchupsStore();
                        this.WinnersBracket = createPlayoffBracket(response, rootStore.Rosters, matchupsStore.Matchups);
                    } else {
                        this.WinnersBracket = createPlayoffBracket(response, rootStore.Rosters);
                    }

                    const firstSeed = rootStore.currentStandings[0];
                    const secondSeed = rootStore.currentStandings[1];

                    addByeWeeks(this.WinnersBracket, firstSeed, secondSeed);
                });
        },
        async getToiletBowl() {
            const rootStore = useRootStore();
            rootStore.IsFetchingData = true;

            return getToiletBowl()
                .then((response) => {
                    if (rootStore.SeasonType == 'post') {
                        const matchupsStore = useMatchupsStore();
                        this.ToiletBowl = createPlayoffBracket(response, rootStore.Rosters, matchupsStore.Matchups);
                    } else {
                        this.ToiletBowl = createPlayoffBracket(response, rootStore.Rosters);
                    }
                });
        },
        async getProjectedWinnersBracket() {
            const rootStore = useRootStore();
            this.WinnersBracket = createProjectedPlayoffBracket(rootStore.playoffTeams);
        },
        async getProjectedToiletBowl() {
            const rootStore = useRootStore();
            this.ToiletBowl = createProjectedToiletBowl(rootStore.lotteryTeams);
        },
    },
});