import { defineStore } from 'pinia';

import SleeperManager from '../interfaces/SleeperManager';
import SleeperRoster from '../interfaces/SleeperRoster';
import SleeperSeason from '../interfaces/SleeperSeason';

import Manager from '../models/Manager';
import Roster from '../models/Roster';

import { SeasonType } from '../types/SeasonType';

import { getManagers, getRosters, getSeasonState } from '../services/SleeperService';
import { sortStandings } from '../services/StandingsService';

interface State {
    IsLoading: boolean;
    IsFetchingData: boolean;
    IsSideNavOpen: boolean;
    CurrentWeek: number;
    CurrentSeason: number;
    SeasonType: SeasonType;
    SeasonStartDate: Date;
    Managers: Manager[];
    Rosters: Roster[];
}

export const useRootStore = defineStore('root', {
    state: (): State => ({
        IsLoading: false,
        IsFetchingData: false,
        IsSideNavOpen: true,
        CurrentWeek: 1,
        CurrentSeason: 2021,
        SeasonStartDate: new Date(),
        SeasonType: 'regular',
        Managers: [],
        Rosters: [],
    }),
    getters: {
        currentStandings(): Roster[] {
            return sortStandings(this.Rosters);
        },
        playoffTeams(): Roster[] {
            return this.currentStandings.slice(0, 6);
        },
        lotteryTeams(): Roster[] {
            return this.currentStandings.slice(6).sort((a, b) => a.TotalPointsScored - b.TotalPointsScored);
        },
        weeksRemainingInSeason(): number {
            return 15 - this.CurrentWeek;
        },
        currentChampion(): Manager {
            const championId = "729232385647976448";
            return this.Managers.find((manager) => manager.Id == championId)!;
        },
    },
    actions: {
        async getBasicLeagueData() {
            this.IsLoading = true;

            let sleeperSeason: SleeperSeason;
            let sleeperManagers: SleeperManager[];
            let sleeperRosters: SleeperRoster[];

            const seasonRequest = getSeasonState().then((response) => sleeperSeason = response);
            const managerRequest = getManagers().then((response) => sleeperManagers = response);
            const rosterRequest = getRosters().then((response) => sleeperRosters = response);

            return Promise.all([seasonRequest, managerRequest, rosterRequest])
                .then(() => {
                    this.CurrentWeek = sleeperSeason.week;
                    this.CurrentSeason = Number.parseInt(sleeperSeason.season);
                    this.SeasonStartDate = new Date(Date.parse(sleeperSeason.season_start_date));
                    this.SeasonType = sleeperSeason.season_type;
                    this.Managers = sleeperManagers.map((sleeperManager) => new Manager(sleeperManager));
                    this.Rosters = sleeperRosters.map((sleeperRoster) => new Roster(sleeperRoster, this.Managers));

                    this.IsLoading = false;
                });
        },
    },
});