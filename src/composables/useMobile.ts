import { ref } from 'vue';
import { useBreakpoints } from '@vueuse/core';

export function useMobile() {
    const breakpoints = useBreakpoints({
        tablet: 640,
    });

    const isMobile = ref(breakpoints.smaller('tablet'));

    return {
        isMobile,
    };
}