import { SeasonType } from "../types/SeasonType";

export default interface SleeperSeason {
    week: number;
    season_type: SeasonType;
    season_start_date: string;
    season: string;
    previous_season: string;
    leg: number;
    league_season: string;
    league_create_season: string;
    display_week: number;
}