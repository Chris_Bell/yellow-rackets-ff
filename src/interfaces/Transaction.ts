export default interface Transaction {
    Id: string;
    Status: 'completed' | 'pending';
    StatusUpdated: Date;
    Week: number;
    CreatedOn: Date;
}