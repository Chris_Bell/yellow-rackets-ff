export default interface SleeperPlayoffMatchup {
    r: number;  // round
    m: number;  // matchupId
    t1: number; // rosterId of Team 1
    t2: number; // rosterId of Team 2
    w: number;  // rosterId of winning team
    l: number;  // rosterId of losing team
    t1_from?: { m: number };   // Where t1 comes from, either winner or loser of the match id, necessary to show bracket progression.
    t2_from?: { m: number };   // Where t2 comes from, either winner or loser of the match id, necessary to show bracket progression.
    //p?: number;  // place? final placement? idk
}