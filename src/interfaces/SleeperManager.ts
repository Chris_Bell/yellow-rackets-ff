export default interface SleeperManager {
    user_id: string;
    display_name: string;
    avatar: string;
    metadata: { team_name: string };
}