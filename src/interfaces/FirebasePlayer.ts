export default interface FirebasePlayer {
    Age: number;
    College: string;
    FantasyDataId: number;
    FantasyPositions: string[];
    FirstName: string;
    FullName: string;
    InjuryStatus: string;
    LastName: string;
    RotoWireId: number;
    RotoWorldId: number;
    SleeperId: string;
    SportRadarId: string;
    StatsId: number;
    Team: string;
    YahooId: number;
    YearsExperience: number;
}