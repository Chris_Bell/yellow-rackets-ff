export default interface SleeperPlayer {
    player_id: number;
    first_name: string;
    last_name: string;
    team: string;
    age: number;
    position: string;
}