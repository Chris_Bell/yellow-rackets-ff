import Player from "../models/Player";

export default interface SleeperMatchup {
    matchup_id: number;
    roster_id: number;
    points: number;
    starters: Player[];
    starters_points: number[];
}