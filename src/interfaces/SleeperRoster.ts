export default interface SleeperRoster {
    roster_id: number;
    owner_id: string;
    co_owners: string[];

    settings: {
        wins: number,
        losses: number,
        fpts: number,
        fpts_decimal: number,
        ppts: number,
        ppts_decimal: number,
        fpts_against: number,
        fpts_against_decimal: number
    };

    players: string[];
    starters: string[];
    reserve: string[];
    taxi: string[];
}