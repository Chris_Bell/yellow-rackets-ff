export default interface SleeperTransaction {
    transaction_id: string;
    type: 'trade' | 'free_agent' | 'waiver';
    status: 'completed' | 'pending';
    status_updated: Date;
    settings: { waiver_bid: number } | null;    // only for free agent ones
    roster_ids: number[];
    leg: number; // week
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    drops: any;
    draft_picks: SleeperDraftPick[];
    creator: string;    // user id of initiator of this transaction
    created: Date;
    consenter_ids: number[];    // roster ids of all involved parties that agreed
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    adds: any;
    waiver_budget: unknown[];   // only used in trades
}

export interface SleeperDraftPick {
    season: number;
    round: number;
    roster_id: number;  // original owner
    previous_owner_id: number;  // previous owner in the context of a trade
    owner_id: number;   // new owner of this pick in the context of a trade
}