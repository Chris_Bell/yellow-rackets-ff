export default interface SleeperLeague {
    total_rosters: number;
    status: string;
    sport: 'nfl';
    settings: unknown;
    season_type: string;
    scoring_settings: unknown;
    roster_positions: unknown;
    previous_league_id: null;
    name: string;
    league_id: string;
    draft_id: string;
    avatar: string;
}