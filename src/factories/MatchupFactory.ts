import SleeperMatchup from "../interfaces/SleeperMatchup";
import Matchup from "../models/Matchup";
import Roster from "../models/Roster";

export function createMatchups(sleeperMatchups: SleeperMatchup[], rosters: Roster[]): Matchup[] {
    const matchupIds = new Set(sleeperMatchups.map((matchup) => matchup.matchup_id));
    const toReturn: Matchup[] = [];

    matchupIds.forEach((id: number) => {
        const matchups = sleeperMatchups.filter((matchup) => matchup.matchup_id == id);
        const rostersForMatchupId = matchups.map((matchup) => rosters.find((roster) => roster.Id == matchup.roster_id)!);
        toReturn.push(new Matchup(matchups, rostersForMatchupId));
    });

    return toReturn;
}