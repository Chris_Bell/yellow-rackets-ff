import SleeperPlayoffMatchup from "../interfaces/SleeperPlayoffMatchup";
import Matchup from "../models/Matchup";
import PlayoffRound from "../models/PlayoffRound";
import Roster from "../models/Roster";
import { sortStandings } from "../services/StandingsService";

export default function createPlayoffBracket(matchups: SleeperPlayoffMatchup[], rosters: Roster[], matchupsThisWeek?: Matchup[]): PlayoffRound[] {
    const rounds = Array.from(new Set(matchups.map((matchup) => matchup.r)));

    const playoffRounds = rounds.map((round) => {
        const roundMatchups = matchups.filter((matchup) => matchup.r == round);

        return new PlayoffRound(roundMatchups, rosters, matchupsThisWeek);
    });

    return playoffRounds;
}

export function addByeWeeks(rounds: PlayoffRound[], firstSeed: Roster, secondSeed: Roster): void {
    const firstRound = rounds.find((round) => round.Round == 1)!;

    firstRound.Matchups.unshift({
        Id: -1,
        TeamA: firstSeed,
        TeamB: null,
        IsBye: true,
    });

    firstRound.Matchups.push({
        Id: -2,
        TeamA: null,
        TeamB: secondSeed,
        IsBye: true,
    });

}

export function createProjectedPlayoffBracket(playoffTeams: Roster[]): PlayoffRound[] {
    const toReturn: PlayoffRound[] = [];

    const firstRound: PlayoffRound = {
        Round: 1,
        Matchups: [
            {
                Id: 1,
                TeamA: playoffTeams[0],
                TeamB: null,
                IsBye: true,
                TeamASeed: 1,
            },
            {
                Id: 2,
                TeamA: playoffTeams[3],
                TeamB: playoffTeams[4],
                IsBye: false,
                TeamASeed: 4,
                TeamBSeed: 5,
            },
            {
                Id: 3,
                TeamA: playoffTeams[2],
                TeamB: playoffTeams[5],
                IsBye: false,
                TeamASeed: 3,
                TeamBSeed: 6,
            },
            {
                Id: 4,
                TeamA: null,
                TeamB: playoffTeams[1],
                IsBye: true,
                TeamBSeed: 2,
            },
        ],
    };

    const secondRound: PlayoffRound = {
        Round: 2,
        Matchups: [
            {
                Id: 1,
                TeamA: playoffTeams[0],
                TeamB: null,
                IsBye: false,
                TeamASeed: 1,
            },
            {
                Id: 2,
                TeamA: null,
                TeamB: playoffTeams[1],
                IsBye: false,
                TeamBSeed: 2,
            },
            {
                Id: 3,
                TeamA: null,
                TeamB: null,
                IsBye: false,
            },
        ],
    };

    const thirdRound: PlayoffRound = {
        Round: 3,
        Matchups: [
            {
                Id: 1,
                TeamA: null,
                TeamB: null,
                IsBye: false,
            },
            {
                Id: 2,
                TeamA: null,
                TeamB: null,
                IsBye: false,
            },
        ],
    };

    toReturn.push(firstRound, secondRound, thirdRound);

    return toReturn;
}

export function createProjectedToiletBowl(unsortedToiletBowlTeams: Roster[]): PlayoffRound[] {
    const toiletBowlTeams = sortStandings(unsortedToiletBowlTeams);
    const toReturn: PlayoffRound[] = [];

    const firstRound: PlayoffRound = {
        Round: 1,
        Matchups: [
            {
                Id: 1,
                TeamA: toiletBowlTeams[5],
                TeamB: null,
                IsBye: true,
                TeamASeed: 12,
            },
            {
                Id: 2,
                TeamA: toiletBowlTeams[0],
                TeamB: toiletBowlTeams[3],
                IsBye: false,
                TeamASeed: 7,
                TeamBSeed: 10,
            },
            {
                Id: 3,
                TeamA: toiletBowlTeams[1],
                TeamB: toiletBowlTeams[2],
                IsBye: false,
                TeamASeed: 8,
                TeamBSeed: 9,
            },
            {
                Id: 4,
                TeamA: null,
                TeamB: toiletBowlTeams[4],
                IsBye: true,
                TeamBSeed: 11,
            },
        ],
    };

    const secondRound: PlayoffRound = {
        Round: 2,
        Matchups: [
            {
                Id: 1,
                TeamA: toiletBowlTeams[5],
                TeamB: null,
                IsBye: false,
                TeamASeed: 12,
            },
            {
                Id: 2,
                TeamA: null,
                TeamB: toiletBowlTeams[4],
                IsBye: false,
                TeamBSeed: 11,
            },
            {
                Id: 3,
                TeamA: null,
                TeamB: null,
                IsBye: false,
            },
        ],
    };

    const thirdRound: PlayoffRound = {
        Round: 3,
        Matchups: [
            {
                Id: 1,
                TeamA: null,
                TeamB: null,
                IsBye: false,
            },
            {
                Id: 2,
                TeamA: null,
                TeamB: null,
                IsBye: false,
            },
        ],
    };

    toReturn.push(firstRound, secondRound, thirdRound);

    return toReturn;
}