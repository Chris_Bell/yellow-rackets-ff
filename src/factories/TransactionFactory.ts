import SleeperTransaction from "../interfaces/SleeperTransaction";
import Transaction from "../interfaces/Transaction";
import Manager from "../models/Manager";
import Roster from "../models/Roster";
import Trade from "../models/Trade";
import WaiverPickup from "../models/WaiverPickup";

export function createTransactions(sleeperTransactions: SleeperTransaction[], rosters: Roster[], managers: Manager[]): Transaction[] {
    return sleeperTransactions.map((transaction) => {
        if (transaction.type == 'trade') {
            return new Trade(transaction, rosters, managers);
        }
        else {
            return new WaiverPickup(transaction, rosters, managers);
        }
    });
}