import PlayerMove from "../interfaces/PlayerMove";
import SleeperTransaction from "../interfaces/SleeperTransaction";

export function createPlayerMovesForTransaction(transaction: SleeperTransaction): PlayerMove[] {
    const toReturn: PlayerMove[] = [];

    const adds = Object.keys(transaction.adds ?? {}).map((key) => [key, transaction.adds[key]]);
    const drops = Object.keys(transaction.drops ?? {}).map((key) => [key, transaction.drops[key]]);

    toReturn.push(...adds.map((add) => {
        return {
            Recipient: Number.parseInt(add[1]),
            Player: Number.parseInt(add[0]),
        };
    }));

    toReturn.push(...drops.map((drop) => {
        return {
            Sender: Number.parseInt(drop[1]),
            Player: Number.parseInt(drop[0]),
        };
    }));

    return toReturn;
}