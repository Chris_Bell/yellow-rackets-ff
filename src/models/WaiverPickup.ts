/* eslint-disable @typescript-eslint/no-non-null-assertion */
import PlayerMove from "../interfaces/PlayerMove";
import SleeperTransaction from "../interfaces/SleeperTransaction";
import Transaction from "../interfaces/Transaction";
import { createPlayerMovesForTransaction } from "../factories/PlayerMoveFactory";
import Manager from "./Manager";
import Roster from "./Roster";

export default class WaiverPickup implements Transaction {
    Id: string;
    Status: 'completed' | 'pending';
    StatusUpdated: Date;
    PlayerMoves: PlayerMove[];
    Roster: Roster;
    Manager: Manager;
    Week: number;
    CreatedOn: Date;
    FaabBid?: number;

    constructor(pickup: SleeperTransaction, rosters: Roster[], managers: Manager[]) {
        this.Id = pickup.transaction_id;
        this.Status = pickup.status;
        this.StatusUpdated = new Date(pickup.status_updated);
        this.Roster = rosters.find((roster) => roster.Id == pickup.roster_ids[0])!;
        this.Manager = managers.find((manager) => manager.Id == this.Roster.Owner.Id)!;
        this.Week = pickup.leg;
        this.PlayerMoves = createPlayerMovesForTransaction(pickup);
        this.CreatedOn = new Date(pickup.created);

        if (pickup.settings?.waiver_bid) {
            this.FaabBid = pickup.settings.waiver_bid;
        }
    }
}