import SleeperPlayoffMatchup from "../interfaces/SleeperPlayoffMatchup";
import Matchup from "./Matchup";
import Roster from "./Roster";

export default class PlayoffMatchup {
    Id: number;
    TeamA: Roster | null;
    TeamB: Roster | null;
    IsBye = false;
    TeamASeed?: number;
    TeamBSeed?: number;
    TeamAFrom?: Matchup;
    TeamBFrom?: Matchup;
    Matchup?: Matchup;

    constructor(sleeperPlayoffMatchup: SleeperPlayoffMatchup, rosters: Roster[], matchup?: Matchup) {
        this.Id = sleeperPlayoffMatchup.m;
        this.TeamA = rosters.find((roster) => roster.Id == sleeperPlayoffMatchup.t1)!;
        this.TeamB = rosters.find((roster) => roster.Id == sleeperPlayoffMatchup.t2)!;

        if (matchup) {
            this.Matchup = matchup;
        }
    }
}