/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { SleeperDraftPick } from "../interfaces/SleeperTransaction";
import Roster from "./Roster";

export default class DraftPick {
    Season: number;
    Round: number;
    Owner: Roster;
    OriginalOwner: Roster;
    PreviousOwner?: Roster; // only used in context of trades

    constructor(pick: SleeperDraftPick, rosters: Roster[]) {
        this.Season = pick.season;
        this.Round = pick.round;
        this.Owner = rosters.find((roster) => roster.Id == pick.owner_id)!;
        this.OriginalOwner = rosters.find((roster) => roster.Id == pick.roster_id)!;

        if (pick.previous_owner_id) {
            this.PreviousOwner = rosters.find((roster) => roster.Id == pick.previous_owner_id)!;
        }
    }
}