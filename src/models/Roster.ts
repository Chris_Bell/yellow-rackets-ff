import SleeperRoster from "../interfaces/SleeperRoster";
import Manager from "./Manager";

export default class Roster {
    Id: number;
    Owner: Manager;
    CoOwners?: Manager[];
    Name: string;

    Wins: number;
    Losses: number;
    WinPercentage: number;
    PointsScored: number;
    TotalPointsScored: number;
    PointsAgainst: number;

    // Players: Player[];
    // Starters: Player[];
    // InjuredReserve: Player[];
    // TaxiSquad: Player[];

    constructor(roster: SleeperRoster, managers: Manager[]) {
        this.Id = roster.roster_id;
        this.Owner = managers.find((manager) => manager.Id == roster.owner_id)!;
        this.CoOwners = managers.filter((manager) => roster.co_owners?.some((coOwner: string) => manager.Id == coOwner));
        this.Name = this.Owner.TeamName ?? `Team ${this.Owner.DisplayName}`;
        this.Wins = roster.settings.wins;
        this.Losses = roster.settings.losses;
        this.WinPercentage = +(this.Wins / (this.Wins + this.Losses)).toFixed(2);
        this.PointsScored = roster.settings.fpts + (roster.settings.fpts_decimal / 100);    // why on earth do they do this
        this.TotalPointsScored = roster.settings.ppts + (roster.settings.ppts_decimal / 100);
        this.PointsAgainst = roster.settings.fpts_against + (roster.settings.fpts_against_decimal / 100);
        // this.Players = roster.players;
        // this.Starters = roster.starters;
        // this.InjuredReserve = roster.reserve;
        // this.TaxiSquad = roster.taxi;
    }
}