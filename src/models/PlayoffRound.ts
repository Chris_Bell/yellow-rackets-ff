import SleeperPlayoffMatchup from "../interfaces/SleeperPlayoffMatchup";
import Matchup from "./Matchup";
import PlayoffMatchup from "./PlayoffMatchup";
import Roster from "./Roster";

export default class PlayoffRound {
    Round: number;
    Matchups: PlayoffMatchup[];

    constructor(matchups: SleeperPlayoffMatchup[], rosters: Roster[], matchupsThisWeek?: Matchup[]) {
        this.Round = matchups[0].r;
        this.Matchups = matchups.map((matchup) => {

            if (matchupsThisWeek) {
                const thisWeeksMatchup = matchupsThisWeek.find((wm) => wm.Id = matchup.m);

                return new PlayoffMatchup(matchup, rosters, thisWeeksMatchup);
            } else {
                return new PlayoffMatchup(matchup, rosters);
            }

        });
    }
}