import SleeperMatchup from "../interfaces/SleeperMatchup";
import Roster from "./Roster";

export default class Matchup {
    Id: number;
    TeamA: Roster;
    TeamB: Roster;
    TeamAPoints: number;
    TeamBPoints: number;
    Winner?: Roster;
    Loser?: Roster;

    constructor(matchups: SleeperMatchup[], rosters: Roster[]) {
        this.Id = matchups[0].matchup_id;
        this.TeamA = rosters[0];
        this.TeamB = rosters[1];
        this.TeamAPoints = matchups[0].points;
        this.TeamBPoints = matchups[1].points;
    }
}