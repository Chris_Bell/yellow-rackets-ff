import { Position } from "../types/Position";
import FirebasePlayer from "../interfaces/FirebasePlayer";

export default class Player {
    Id: string;
    Name: string;
    Team: string;
    Age: number;
    Position: Position[];
    College: string;
    YearsExperience: number;

    constructor(player: FirebasePlayer) {
        this.Id = player.SleeperId;
        this.Name = player.FullName;
        this.Team = player.Team;
        this.Age = player.Age;
        this.Position = player.FantasyPositions as Position[];
        this.College = player.College;
        this.YearsExperience = player.YearsExperience;
    }
}