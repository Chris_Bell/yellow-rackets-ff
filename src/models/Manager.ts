import SleeperManager from "../interfaces/SleeperManager";

export default class Manager {
    Id: string;
    DisplayName: string;
    Avatar: string;
    TeamName: string;

    constructor(manager: SleeperManager) {
        this.Id = manager.user_id;
        this.DisplayName = manager.display_name;
        this.Avatar = manager.avatar;
        this.TeamName = manager.metadata.team_name;
    }
}