/* eslint-disable @typescript-eslint/no-non-null-assertion */
import PlayerMove from "../interfaces/PlayerMove";
import SleeperTransaction from "../interfaces/SleeperTransaction";
import Transaction from "../interfaces/Transaction";
import { createPlayerMovesForTransaction } from "../factories/PlayerMoveFactory";
import DraftPick from "./DraftPick";
import Manager from "./Manager";
import Roster from "./Roster";

export default class Trade implements Transaction {
    Id: string;
    Status: 'completed' | 'pending';
    StatusUpdated: Date;
    Rosters: Roster[];
    Managers: Manager[];
    Week: number;
    PlayerMoves: PlayerMove[];
    DraftPicks: DraftPick[];
    Initiator: Manager;
    CreatedOn: Date;
    // Faab: number;

    constructor(trade: SleeperTransaction, rosters: Roster[], managers: Manager[]) {
        this.Id = trade.transaction_id;
        this.Status = trade.status;
        this.StatusUpdated = new Date(trade.status_updated);
        this.Rosters = rosters.filter((roster) => trade.roster_ids.includes(roster.Id));
        this.Managers = managers.filter((manager) => this.Rosters.some((roster) => roster.Owner == manager));
        this.Week = trade.leg;
        this.DraftPicks = trade.draft_picks.map((pick) => new DraftPick(pick, rosters));
        this.PlayerMoves = createPlayerMovesForTransaction(trade);
        this.Initiator = managers.find((manager) => manager.Id == trade.creator)!;
        this.CreatedOn = new Date(trade.created);
        // this.Faab = trade
    }
}