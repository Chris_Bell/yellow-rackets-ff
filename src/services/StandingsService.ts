import Roster from "../models/Roster";

export function sortStandings(rosters: Roster[]): Roster[] {
    return Array.from(rosters).sort((rosterA, rosterB) => {

        const winPercentageDiff = rosterB.WinPercentage - rosterA.WinPercentage;

        if (winPercentageDiff != 0) { return winPercentageDiff; }

        return rosterB.PointsScored - rosterA.PointsScored;

    });
}