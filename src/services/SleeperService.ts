import SleeperPlayoffMatchup from "../interfaces/SleeperPlayoffMatchup";
import SleeperLeague from "../interfaces/SleeperLeague";
import SleeperManager from "../interfaces/SleeperManager";
import SleeperMatchup from "../interfaces/SleeperMatchup";
import SleeperRoster from "../interfaces/SleeperRoster";
import SleeperSeason from "../interfaces/SleeperSeason";
import SleeperTransaction from "../interfaces/SleeperTransaction";

const leagueId = '729165836115390464';

export async function getSeasonState(): Promise<SleeperSeason> {
    return fetch('https://api.sleeper.app/v1/state/nfl')
        .then(async (response: Response) => await response.json());
}

export async function getLeague(): Promise<SleeperLeague> {
    return fetch(`https://api.sleeper.app/v1/league/${leagueId}`)
        .then(async (response: Response) => await response.json());
}

export async function getManagers(): Promise<SleeperManager[]> {
    return fetch(`https://api.sleeper.app/v1/league/${leagueId}/users`)
        .then(async (response: Response) => await response.json());
}

export async function getRosters(): Promise<SleeperRoster[]> {
    return fetch(`https://api.sleeper.app/v1/league/${leagueId}/rosters`)
        .then(async (response: Response) => await response.json());
}

export async function getMatchups(week: number): Promise<SleeperMatchup[]> {
    return fetch(`https://api.sleeper.app/v1/league/${leagueId}/matchups/${week}`)
        .then(async (response: Response) => await response.json());
}

export async function getWinnersBracket(): Promise<SleeperPlayoffMatchup[]> {
    return fetch(`https://api.sleeper.app/v1/league/${leagueId}/winners_bracket`)
        .then(async (response: Response) => await response.json());
}

export async function getToiletBowl(): Promise<SleeperPlayoffMatchup[]> {
    return fetch(`https://api.sleeper.app/v1/league/${leagueId}/losers_bracket`)
        .then(async (response: Response) => await response.json());
}

export async function getWeeklyProjections(week: number) {
    return fetch(`https://api.sleeper.app/projections/nfl/2021/${week}?season_type=regular&position[]=FLEX&position[]=QB&position[]=RB&position[]=SUPER_FLEX&position[]=TE&position[]=WR&order_by=half_ppr`)
        .then(async (response: Response) => console.log(await response.json()));
}

export async function getTransactions(week: number): Promise<SleeperTransaction[]> {
    return fetch(`https://api.sleeper.app/v1/league/${leagueId}/transactions/${week}`)
        .then(async (response: Response) => await response.json());
}
