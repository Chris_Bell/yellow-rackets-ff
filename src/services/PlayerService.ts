const firebaseUrl = 'https://us-central1-yellow-rackets-ff.cloudfunctions.net';

export async function getPlayer(playerId: string) {
    return fetch(`${firebaseUrl}/players/${playerId}`)
        .then(async (response) => await response.json());
}