export default interface Player {
    Age: number;
    College: string;
    FantasyDataId: number;
    FantasyPositions: string[];
    FirstName: string;
    FullName: string;
    InjuryStatus: string;
    LastName: string;
    RotoWireId: number;
    RotoWorldId: number;
    SleeperId: string;
    SportRadarId: string;
    StatsId: string;
    Team: string;
    YahooId: string;
    YearsExperience: number;
}