import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import createPlayers from './factories/PlayerFactory';
import { getAllPlayers } from './services/SleeperService';

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

admin.initializeApp();

export const importPlayers = functions.https.onRequest(async (request, response) => {
    const playersResponse = await getAllPlayers();
    const playerModels = createPlayers(playersResponse);
    const writes = playerModels.map((player) => admin.firestore().collection('players').add({ original: player }));
    await Promise.all(writes);
    response.json({ result: `${writes.length} added` });
});

export const players = functions.https.onRequest(async (request, response) => {
    const id = request.params[0];
    const q = quer
    const player = await admin.firestore().collection('players').get()
    response.json({ result: request.params });
});