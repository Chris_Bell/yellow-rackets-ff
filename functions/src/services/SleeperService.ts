import axios from "axios";

export async function getAllPlayers(): Promise<any[]> {
    return axios.get('https://api.sleeper.app/v1/players/nfl')
        .then((response) => response.data);
}