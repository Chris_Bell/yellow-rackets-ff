import Player from "../interfaces/Player";

export default function createPlayers(response: any) {
    const toReturn: Player[] = [];

    const playerIds = Object.keys(response);

    playerIds.forEach((id) => {
        const rawObject = response[id];
        const player: Player = {
            SleeperId: rawObject.player_id,
            Age: rawObject.age ?? null,
            College: rawObject.college ?? null,
            FantasyDataId: rawObject.fantasy_data_id ?? null,
            FantasyPositions: rawObject.fantasy_positions ?? null,
            FirstName: rawObject.first_name,
            FullName: `${rawObject.first_name} ${rawObject.last_name}`,
            InjuryStatus: rawObject.injury_status ?? null,
            LastName: rawObject.last_name,
            RotoWireId: rawObject.rotowire_id ?? null,
            RotoWorldId: rawObject.rotoworld_id ?? null,
            SportRadarId: rawObject.sportradar_id ?? null,
            StatsId: rawObject.stats_id ?? null,
            Team: rawObject.team ?? null,
            YahooId: rawObject.yahoo_id ?? null,
            YearsExperience: rawObject.years_exp ?? null,
        };

        toReturn.push(player);
    });

    return toReturn;
}